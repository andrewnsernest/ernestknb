import os
import distutils.dir_util
import shutil

count = 0
filenames = []
titles = []
macros = []

myScriptPath = os.path.dirname(os.path.realpath(__file__))
mySourceFile = 'frame.tex'
myKNuggetFile = 'knugget.tex'
myPackageFile = 'ernestknb.sty'
myBeamerFileEnd = 'Slide.tex'
myArticleFileEnd = 'Narrative.tex'

# 

for (dirname, dirs, files) in os.walk(myScriptPath):
   for filename in files:
       if filename == mySourceFile :
           #print os.path.join(dirname,filename)
           filenames.append(os.path.join(dirname,filename))
           title = os.path.basename(dirname)
           titles.append(title)
           macro = dirname.replace ("/home/anernest/Documents/", "")
           macro = macro.replace (" ", "")
           macro = macro.replace (os.sep, "")
           macros.append(macro)
           print macro
           myBeamerFile = title + " " + myBeamerFileEnd
           myArticleFile = title + " " + myArticleFileEnd
           with open(os.path.join(dirname,myBeamerFile), 'w') as beamerfile:
              articlefile = open(os.path.join(dirname,myArticleFile), 'w')
              knuggetfile = open(os.path.join(dirname,myKNuggetFile), 'w')
              beamerfile.write("\documentclass[class=beamer, crop=false, ignorenonframetext]{standalone}\n")
              articlefile.write("\documentclass[class=article, crop=false]{standalone}\usepackage{beamerarticle}\n")
              knuggetfile.write("\documentclass[class=article, crop=false]{standalone}\usepackage{beamerarticle}\n")
              preamble = open('preamble.tex', 'r')
              with preamble as infile:
                for line in infile:
                   beamerfile.write(line.replace("The Frame Title",title))
                   articlefile.write(line.replace("The Frame Title",title))
                   knuggetfile.write(line.replace("The Frame Title",title))
                infile.close()
              #with open(os.path.join(dirname,filename)) as infile:
                #for line in infile:
                   #beamerfile.write(line)
                   #articlefile.write(line)
                   #knuggetfile.write(line)
                #infile.close()
              knuggetfile.write("\n\\input{" + filename + "}\n")
              postamble = open('postamble.tex', 'r')
              with postamble as infile:
                for line in infile:
                   beamerfile.write(line)
                   articlefile.write(line)
                   knuggetfile.write(line)
                infile.close()
              beamerfile.close()
              articlefile.close()
              knuggetfile.close()
           titles.append(title)
           macros.append(macro)
           count = count + 1
#print filenames
print 'Files:', count

with open(myPackageFile, 'w') as pkgfile:
    for fname in filenames:
        pkgfile.write("\\newcommand{\\")
        pkgfile.write(macros[filenames.index(fname)])
        pkgfile.write("}{\n")
        pkgfile.write("\\begin{frame}\\frametitle{")
        pkgfile.write(titles[filenames.index(fname)])
        pkgfile.write("}\n")
        with open(fname) as infile:
            for line in infile:
                pkgfile.write(line)
        pkgfile.write("\n}\n\\end{frame}\n}\n")

if os.path.isfile(".git"):
   shutil.copy(myPackageFile,os.path.join(os.pardir,myPackageFile))

#if os.path.isdir(myDroidtexmfPath):
#    print "Droid:", myDroidtexmfPath
#    distutils.dir_util.copy_tree(myScriptPath,myDroidtexmfPath, preserve_mode=0, preserve_times=0 )
#if os.path.isdir(myLinuxtexmfPath):
#    print "Linux:", myLinuxtexmfPath
#    distutils.dir_util.copy_tree(myScriptPath,myLinuxtexmfPath, preserve_mode=0, preserve_times=0)
#if os.path.exists(myProjectFile):
#    print "Project:", myProjectFile
#    shutil.copy("ernestgls.tex",myProjectFile)